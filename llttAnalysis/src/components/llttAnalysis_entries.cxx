#include "../BaselineVarsllttAlg.h"
#include "../LeptonPairDecoratorAlg.h"
#include "../MMCSelectorAlg.h"
#include "../HllttSelectorAlg.h"

using namespace HLLTT;

DECLARE_COMPONENT(BaselineVarsllttAlg)
DECLARE_COMPONENT(LeptonPairDecoratorAlg)
DECLARE_COMPONENT(MMCSelectorAlg)
DECLARE_COMPONENT(HllttSelectorAlg)
